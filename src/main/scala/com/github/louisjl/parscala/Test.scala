package com.github.louisjl.parscala

object Test {
    def main(args: Array[String]): Unit =
        println(number.parse("'134'"))

    private def number = string map (_.toInt)
    private def string = drop(quote) ~ contents ~ drop(quote)
    private def contents = character.* compose ("abc" + (_: String))
    private def character = notSpecial ~ take(1)
    private def notSpecial = special.! | drop(escape)
    private def special = escape | quote
    private val quote = "\'"
    private val escape = "\\"

    private val p2 = 11 times "a"
}
