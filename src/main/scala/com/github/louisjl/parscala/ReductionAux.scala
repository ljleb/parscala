package com.github.louisjl.parscala

sealed trait ReductionAux[I, O] {
    def toReduction[S](self: Parser[S, I, O]): Parser[S, O, O]
}

object ReductionAux {
    implicit def unitReduction[O](implicit combiner: Combiner[O, O, O]): ReductionAux[Unit, O] = new ReductionAux[Unit, O] {
        override def toReduction[S](self: Parser[S, Unit, O]): Parser[S, O, O] =
            toSequent(self)
    }

    implicit def identity[O]: ReductionAux[O, O] = new ReductionAux[O, O] {
        override def toReduction[S](self: Parser[S, O, O]): Parser[S, O, O] =
            self
    }
}
