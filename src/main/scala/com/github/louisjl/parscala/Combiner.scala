package com.github.louisjl.parscala

import scala.collection.SeqLike
import scala.collection.generic.CanBuildFrom

trait Combiner[-L, -R, +C] {
    def combine(left: L, right: R): Either[ParseException, C]
}

object Combiner {
    implicit def SeqLikeCombiner[A, R](implicit cbf: CanBuildFrom[R, A, R], ev: R => SeqLike[A, R]): Combiner[R, R, R] =
        (left: R, right: R) => Right(left ++ right)

    implicit def unitCombiner[C]: Combiner[C, Unit, C] =
        (left: C, _: Unit) => Right(left)
}
