package com.github.louisjl.parscala

import scala.collection.GenTraversableLike

trait SourceAux[S] {
    def matchLength(source: S, length: Int): Either[ParseException, (S, S)]
    def tryRemoveStart(slice: S, source: S): Either[ParseException, S]
}

object SourceAux {
    implicit def traversableSource[A, C](implicit asTraversable: C => GenTraversableLike[A, C]): SourceAux[C] = new SourceAux[C] {
        override def matchLength(source: C, length: Int): Either[ParseException, (C, C)] = {
            val traversableSource = asTraversable(source)
            if(traversableSource.size < length) Left(new ParseException(traversableSource, s"$length"))
            else {
                val splitSource = traversableSource.splitAt(length)
                Right(splitSource._1, splitSource._2)
            }
        }

        override def tryRemoveStart(slice: C, source: C): Either[ParseException, C] = {
            val traversableSlice = asTraversable(slice)
            val traversableSource = asTraversable(source)
            val splitSource = traversableSource.splitAt(traversableSlice.size)
            if(!splitSource._1.equals(slice)) Left(new ParseException(traversableSource, traversableSlice.mkString("\"", "", "\"")))
            else Right(splitSource._2)
        }
    }
}
